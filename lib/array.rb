class Array
  def group_by_length
    element_collection = Hash.new { |hash, key| hash[key] = [] }
    for value in self
      element_collection[value.to_s.length].push(value)
    end
    element_collection
  end
end

